Function.prototype.extend = function(parent) {
	var parent_interface = parent.constructor === Function ? parent.prototype : parent;
	this.prototype = Object.create(parent_interface);
	this.prototype.constructor = this;
	this.prototype.parent = parent_interface;

	return this;
}

var GUI = {
}

GUI.Element = {
    init: function(params) {
		var self = this.enhance(this.el);
		self.params = params;
        if (self.params !== undefined) {
            if (!(self.params instanceof Object)) {
                self.setAttribute("id", self.params);
            } else if (self.params.id !== undefined) {
                self.setAttribute("id", self.params.id);
            }
        }
		return self;
    },
	enhance: function(el) {
		Object.defineProperty(el, "appendTo", {
			value: function(parent) {
				parent.appendChild(this);
				return this;
			}
		});
		Object.defineProperty(el, "placeAt", {
			value: function(parent) {
				while (parent.hasChildNodes()) {
					parent.removeChild(parent.lastChild);
				}
				parent.appendChild(this);
				return this;
			}
		});
		Object.defineProperty(el, "attr", {
			value: function(name, value) {
				if (value !== undefined) {
					this.setAttribute(name, value);
					return this;
				} else {
					return this.getAttribute(name);
				}
			}
		});
		Object.defineProperty(el, "hasParam", {
			value: function(name) {
				return ((typeof this.params != "undefined") && (this.params[name] !== undefined));
			}
		});
		Object.defineProperty(el, "getTextParam", {
			value: function(name) {
				return (this.hasParam(name) ? this.params[name] : "");
			}
		});
		Object.defineProperty(el, "event", {
			value: function(event, f) {
				this.addEventListener(event, f, false);
				return this;
			}
		});
		return el;
	}
}

GUI.Body = function(params) {
	this.createElement();
	return this.init();
}
GUI.Body.extend(GUI.Element);
GUI.Body.prototype.createElement = function() {
	this.el = document.body;
}

GUI.Form = function(params) {
	this.createElement();
	return this.init();
}
GUI.Form.extend(GUI.Element);
GUI.Form.prototype.createElement = function() {
	this.el = document.createElement("form");
}

GUI.Input = function(params) {
	this.createElement();
	return this.init();
}
GUI.Input.extend(GUI.Element);
GUI.Input.prototype.createElement = function() {
	this.el = document.createElement("input");
}

GUI.TextInput = function(params) {
	this.createElement();
	return this.init(params);
}
GUI.TextInput.extend(GUI.Input);
GUI.TextInput.prototype.init = function(params) {
	var self = this.parent.init.call(this, params);
	return self.attr("type", "text");
}

GUI.ButtonInput = function(params) {
	this.createElement();
	return this.init(params);
}
GUI.ButtonInput.extend(GUI.Input);
GUI.ButtonInput.prototype.init = function(params) {
	var self = this.parent.init.call(this, params);
	return self.attr("type", "button");
}

GUI.Label = function(params) {
	this.createElement();
	return this.init(params);
	//return self.text(self.getTextParam("text"));
}
GUI.Label.extend(GUI.Element);

GUI.Label.prototype.createElement = function() {
	this.el = document.createElement("label");
}
GUI.Label.prototype.init = function(params) {
	var self = this.parent.init.call(this);
	self.params = params;
	return self.setText(params);
}
GUI.Label.prototype.enhance = function(el) {
	el = this.parent.enhance.call(this, this.el);
	Object.defineProperty(el, "for", {
		value: function(destination) {
			var id;
			if (destination instanceof Object) {
				id = destination.getAttribute("id");
			} else {
				id = destination;
			}
			this.setAttribute("for", id);
			return this;
		}
	});
	Object.defineProperty(el, "setText", {
		value: function(text) {
			this.appendChild(document.createTextNode(text));
			return this;
		}
	});
	return el;
}

GUI.Select = function(params) {
	this.createElement();
	return this.init(params);
}
GUI.Select.extend(GUI.Element);
GUI.Select.prototype.createElement = function() {
	this.el = document.createElement("select");
}
GUI.Select.prototype.init = function(params) {
	var self = this.parent.init.call(this);
	self.params = params;
	if (self.hasParam("options")) {
		for (var i = 0; i < self.params.options.length; i++) {
			self.addOption(self.params.options[i]);	
		}
	}
	return self;
}
GUI.Select.prototype.enhance = function(el) {
	el = this.parent.enhance.call(this, el);
	Object.defineProperty(el, "addOption", {
		value: function(option) {
			var option = new GUI.Option({text: option[0], value: option[1]}).appendTo(this);
			return this;
		}
	});
	return el;
}

// Option

GUI.Option = function(params) {
	this.createElement();
	return this.init(params);
}
GUI.Option.extend(GUI.Element);
GUI.Option.prototype.createElement = function() {
	this.el = document.createElement("option");
}
GUI.Option.prototype.init = function(params) {
	var self = this.parent.init.call(this);
	self.params = params;
	return self.attr("value", self.getTextParam("value")).setText(self.getTextParam("text"));
}
GUI.Option.prototype.enhance = function(el) {
	el = this.parent.enhance.call(this, el);
	Object.defineProperty(el, "setText", {
		value: function(text) {
			this.appendChild(document.createTextNode(text));
			return this;
		}
	});
	return el;
}


// Table

GUI.Table = function(params) {
	this.createElement();
	return this.init(params);
}
GUI.Table.extend(GUI.Element);
GUI.Table.prototype.createElement = function() {
	this.el = document.createElement("table");
}
GUI.Table.prototype.init = function(params) {
	var self = this.parent.init.call(this);
	self.params = params;
	for (var row, i = 0; i < self.params.rows; i++) {
		//self.addRow({ columns: self.params.columns });
		row = self.insertRow(-1);
		for (var j = 0; j < self.params.columns; j++) {
			row.insertCell(-1);
		}
	}
	return self;
}
GUI.Table.prototype.enhance = function(el) {
	el = this.parent.enhance.call(this, el);
	Object.defineProperty(el, "addRow", {
		value: function(n_columns) {
			var table_row = new GUI.TableRow(n_columns).appendTo(this);
			return this;
		}
	});
	Object.defineProperty(el, "forEachRow", {
		value: function(f) {
			for (var i = 0; i < this.rows.length; i++) {
				var row = this.rows[i];
				f(i, row);
			}
			return this;
		}
	});
	Object.defineProperty(el, "forEachColumn", {
		value: function(row, f) {
			for (var i = 0; i < row.cells.length; i++) {
				var cell = row.cells[i];
				f(i, cell);
			}
			return this;
		}
	});
	Object.defineProperty(el, "forEachCell", {
		value: function(f) {
			for (var i = 0; i < this.rows.length; i++) {
				var row = this.rows[i];
				for (var j = 0; j < row.cells.length; j++) {
					var cell = row.cells[j];
					f(i * row.cells.length + j, cell);
				}
			}
			return this;
		}
	});
	return el;
}

// TableRow

GUI.TableRow = function(params) {
	this.createElement();
	return this.init(params);
}
GUI.TableRow.extend(GUI.Element);
GUI.TableRow.prototype.createElement = function() {
	this.el = document.createElement("tr");
}
GUI.TableRow.prototype.init = function(params) {
	var self = this.parent.init.call(this);
	self.params = params;
	for (var i = 0; i < self.params.columns; i++) {
		self.addColumn();
	}
	return self;
}
GUI.TableRow.prototype.enhance = function(el) {
	el = this.parent.enhance.call(this, el);
	Object.defineProperty(el, "addColumn", {
		value: function() {
			var table_column = new GUI.TableColumn().appendTo(this);
			return this;
		}
	});
	return el;
}

// TableColumn

GUI.TableColumn = function(params) {
	this.createElement();
	return this.init(params);
}
GUI.TableColumn.extend(GUI.Element);
GUI.TableColumn.prototype.createElement = function() {
	this.el = document.createElement("td");
}
